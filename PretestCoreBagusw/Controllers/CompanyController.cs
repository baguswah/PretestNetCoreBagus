﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using PretestCoreBagusw.Models;
using PretestCoreBagusw.Repository;
using System.Web.Http.ModelBinding;

namespace PretestCoreBagusw.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public CompanyController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("CompanyList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getCompany()
        {
            var result = _authentication.getCompanyList<ModelCompany>();

            return Ok(result);
        }



        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelCompany company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.Int64);
            dp_param.Add("createdby", company.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_CreateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = company });
            }

            return BadRequest(result);
        }
        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelCompany company, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.Int64);
            dp_param.Add("createdby", company.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_UpdateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = company });
            }

            return BadRequest(result);
        }











    }



















}