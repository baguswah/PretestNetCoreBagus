﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using PretestCoreBagusw.Models;
using PretestCoreBagusw.Repository;


namespace preTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentCategoryController : ControllerBase
    {
        private readonly IJWTAuthManager _authentication;
        public DocumentCategoryController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("DocumentCategoryList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getPosition()
        {
            var result = _authentication.getDocumentCategoryList<ModelDocumentCategory>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocumentCategory documentCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", documentCategory.Name, DbType.String);
            dp_param.Add("createdby", documentCategory.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_CreateDocumentCategory", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = documentCategory });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocumentCategory documentCategory, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", documentCategory.Name, DbType.String);
            dp_param.Add("createdby", documentCategory.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_UpdateDocumentCategory", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = documentCategory });
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_DeleteDocumentCategory", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
