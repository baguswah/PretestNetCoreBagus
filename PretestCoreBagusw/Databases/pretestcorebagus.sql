USE [master]
GO
/****** Object:  Database [PretestBagusWahyu]    Script Date: 8/4/2023 8:52:06 AM ******/
CREATE DATABASE [PretestBagusWahyu] ON  PRIMARY 
( NAME = N'PretestBagusWahyu', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\PretestBagusWahyu.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PretestBagusWahyu_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\PretestBagusWahyu_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PretestBagusWahyu].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ARITHABORT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PretestBagusWahyu] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PretestBagusWahyu] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PretestBagusWahyu] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PretestBagusWahyu] SET  MULTI_USER 
GO
ALTER DATABASE [PretestBagusWahyu] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PretestBagusWahyu] SET DB_CHAINING OFF 
GO
USE [PretestBagusWahyu]
GO
/****** Object:  Table [dbo].[TBCompany]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBCompany](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBCompany] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBDocument]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBDocument](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocument] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBDocumentCategory]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBDocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocumentCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBPosition]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBPosition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBPosition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBUser]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_Document]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Document]
AS
SELECT dbo.TBDocument.IDCompany, dbo.TBDocument.IDCategory, dbo.TBDocument.Name, dbo.TBDocument.Description, dbo.TBDocument.Flag, dbo.TBDocument.CreatedBy, dbo.TBDocument.CreatedAt, 
                  dbo.TBDocumentCategory.Name AS Expr1, dbo.TBCompany.Name AS Expr2
FROM     dbo.TBDocument INNER JOIN
                  dbo.TBDocumentCategory ON dbo.TBDocument.IDCategory = dbo.TBDocumentCategory.ID INNER JOIN
                  dbo.TBCompany ON dbo.TBDocument.IDCompany = dbo.TBCompany.ID
GO
/****** Object:  View [dbo].[View_User]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_User]
AS
SELECT dbo.TBUser.IDCompany, dbo.TBUser.IDPosition, dbo.TBUser.Address, dbo.TBUser.Name, dbo.TBUser.Email, dbo.TBUser.Telephone, dbo.TBUser.Username, dbo.TBUser.Password, dbo.TBUser.Role, dbo.TBUser.Flag, 
                  dbo.TBUser.CreatedBy, dbo.TBUser.CreatedAt, dbo.TBCompany.Name AS Expr1, dbo.TBPosition.Name AS Expr2
FROM     dbo.TBCompany INNER JOIN
                  dbo.TBDocument ON dbo.TBCompany.ID = dbo.TBDocument.IDCompany INNER JOIN
                  dbo.TBDocumentCategory ON dbo.TBDocument.IDCategory = dbo.TBDocumentCategory.ID INNER JOIN
                  dbo.TBPosition ON dbo.TBCompany.ID = dbo.TBPosition.ID INNER JOIN
                  dbo.TBUser ON dbo.TBCompany.ID = dbo.TBUser.IDCompany AND dbo.TBPosition.ID = dbo.TBUser.IDPosition
GO
SET IDENTITY_INSERT [dbo].[TBCompany] ON 

INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, N'28afa48f-2292-45c1-a48e-31fe719df0c0', N'Samsan-Tech', N'South Korea', N'samsan@tech.com', N'56945', 333, 1, CAST(N'2023-08-03T11:52:12.550' AS DateTime))
INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (7, N'34a9428f-ca59-4a68-8210-1bc8d18ed59a', N'StarTech', N'galaxy', N'star@mail.com', N'34341', 22, 1, CAST(N'2023-08-03T15:35:13.907' AS DateTime))
INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (12, N'3ddfac03-38b7-45df-a0f8-a08fe0ed1310', N'X-tech', N'valley', N'x@mail.com', N'9393', 0, 0, CAST(N'2023-08-04T01:41:27.620' AS DateTime))
INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (13, N'ef6a4064-3d97-42b4-8ee6-e01c886c02b0', N'BRealCompany', N'Jogja', N'Bcom@mail.com', N'34341', 0, 0, CAST(N'2023-08-04T01:53:36.930' AS DateTime))
SET IDENTITY_INSERT [dbo].[TBCompany] OFF
GO
SET IDENTITY_INSERT [dbo].[TBDocument] ON 

INSERT [dbo].[TBDocument] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, N'ceabf7fa-59e8-42db-934c-d149090ac6a3', 1, 1, N'FIRST', N'Things', 0, 0, CAST(N'2023-08-04T01:47:35.370' AS DateTime))
SET IDENTITY_INSERT [dbo].[TBDocument] OFF
GO
SET IDENTITY_INSERT [dbo].[TBDocumentCategory] ON 

INSERT [dbo].[TBDocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'6e08ea65-c117-4253-898a-3a7c60a5f2a7', N'ERP RESOURCE', 0, CAST(N'2023-08-03T12:58:40.753' AS DateTime))
INSERT [dbo].[TBDocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (3, N'3e46efac-dcb5-4a73-ae82-e95ead707c94', N'CloseBeta', 0, CAST(N'2023-08-04T01:48:33.313' AS DateTime))
INSERT [dbo].[TBDocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (4, N'16c95892-2bf8-40cb-9a7d-580e7011239e', N'OpenSource', 0, CAST(N'2023-08-04T01:48:34.130' AS DateTime))
SET IDENTITY_INSERT [dbo].[TBDocumentCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[TBPosition] ON 

INSERT [dbo].[TBPosition] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'bf0b6ce4-bdbe-437b-a175-517086c48f4c', N'SecondPosition', 0, CAST(N'2023-08-04T01:49:28.593' AS DateTime))
INSERT [dbo].[TBPosition] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'cd3be04a-b4aa-42e6-8c89-119e1a461b0b', N'Fourthposition', 0, CAST(N'2023-08-04T01:49:53.390' AS DateTime))
SET IDENTITY_INSERT [dbo].[TBPosition] OFF
GO
SET IDENTITY_INSERT [dbo].[TBUser] ON 

INSERT [dbo].[TBUser] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (10, N'56b9c6c5-ed2b-4ff7-83eb-3d4a1e4f05e6', 1, 1, N'bagus', N'Jogja', N'b@mail.com', N'6686', N'bgs', N'9E94FCCC942B27B1681215CB2E5E92E9', N'Admin', 0, 0, CAST(N'2023-08-04T01:51:11.617' AS DateTime))
INSERT [dbo].[TBUser] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (11, N'fbbf260c-023c-418e-b97a-045d8f64d385', 1, 1, N'BGS', N'Jogja', N'w@mail.com', N'6686', N'why', N'84D46AB0862ACD127EDEC820CA60F2CB', N'Admin', 0, 0, CAST(N'2023-08-04T01:51:30.540' AS DateTime))
SET IDENTITY_INSERT [dbo].[TBUser] OFF
GO
ALTER TABLE [dbo].[TBDocument]  WITH CHECK ADD  CONSTRAINT [FK_TBDocument_TBCompany] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBDocument] CHECK CONSTRAINT [FK_TBDocument_TBCompany]
GO
ALTER TABLE [dbo].[TBDocument]  WITH CHECK ADD  CONSTRAINT [FK_TBDocument_TBDocumentCategory] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[TBDocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[TBDocument] CHECK CONSTRAINT [FK_TBDocument_TBDocumentCategory]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBCompany] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBCompany]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBPosition] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[TBPosition] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBPosition]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateCompany]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateCompany]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

INSERT INTO [dbo].[TBCompany]
           ([UID]
           ,[Name]
           ,[Address]
		   ,[Email]
		   ,[Telephone]
		   ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
			,@name
			,@address
			,@email
			,@telephone
			,@flag
			,@createdby
           ,GETDATE())

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocument]
    -- Add the parameters for the stored procedure here
    @idcompany int,
    @idcategory int,
    @name varchar(255),
    @description text, 
    @flag int,
    @createdby int,
    @retVal int OUTPUT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO [dbo].[TBDocument]
           ([UID]
           ,[IDCompany]
           ,[IDCategory]
           ,[Name]
           ,[Description]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@idcompany
           ,@idcategory
           ,@name
           ,@description
           ,@flag
           ,@createdby
           ,GETDATE())

    IF(@@ROWCOUNT > 0)
    BEGIN
        SET @retVal = 200
    END
    ELSE
    BEGIN
        SET @retVal = 500
    END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocumentCategory]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocumentCategory]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

INSERT INTO [dbo].[TBDocumentCategory]
           ([UID]
           ,[Name]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@name
		   ,@createdby
           ,GETDATE())

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreatePosition]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreatePosition]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

INSERT INTO [dbo].[TBPosition]
    (    [UID],
		[Name],
		[CreatedBy],
		[CreatedAt]
 )
 VALUES
	(
		NEWID(),
		@name,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateUser]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateUser] 
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [PretestBagusWahyu].[dbo].[TBUser]
         ([UID]
           ,[IDCompany]
           ,[IDPosition]
           ,[Name]
           ,[Address]
           ,[Email]
           ,[Telephone]
           ,[Username]
           ,[Password]
           ,[Role]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID(),
		   @idcompany,
           @idposition,
           @name,
           @address,
           @email,
           @telephone,
           @username,
           CONVERT(VARCHAR(32),HashBytes('MD5',@password),2),
           @role,
           @flag,
           @createdby,
           GETDATE())
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteCompany]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBCompany]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocument]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBDocument]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocumentCategory]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBDocumentCategory]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeletePosition]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeletePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBPosition]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUser]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBUser]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
	@email varchar(50),
	@password varchar(50),
	@retVal int OUTPUT

AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	FROM TBUser
	WHERE 
		[Email] = @email and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateCompany]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here

UPDATE [dbo].[TBCompany] SET
           [Name]=@name
           ,[Address]=@address
           ,[Email]=@email
           ,[Telephone]=@telephone
           ,[Flag]=@flag
           ,[CreatedBy]= @createdby

  WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
    @idcategory int,
    @name varchar(255),
    @description text,
    @flag int,
    @createdby int,
    @retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBDocument] SET
           [IDCompany] = @idcompany
           ,[IDCategory] = @idcategory
           ,[Name] = @name
           ,[Description] = @description
           ,[Flag] = @flag
		   ,[CreatedBy] = @createdby

  WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocumentCategory]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(255),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBDocumentCategory] SET
           [Name] = @name
		   ,[CreatedBy] = @createdby

		   WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
    
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePosition]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBPosition] SET
           [Name] = @name
		   ,[CreatedBy] = @createdby

		   WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
    
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 8/4/2023 8:52:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar (50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBUser] SET
           [IDCompany] = @idcompany
		   ,[IDPosition] = @idposition
		   ,[Name] = @name
           ,[Address] = @address
		   ,[Email] = @email
		   ,[Telephone] = @telephone
		   ,[Username] = @username
		   ,[Password] = CONVERT(varchar(32), HASHBYTES('MD5', @password), 2)
		   ,[Role] = @role
		   ,[Flag] = @flag
		   ,[CreatedBy] = @createdby

  WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -120
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBDocument"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocumentCategory"
            Begin Extent = 
               Top = 139
               Left = 314
               Bottom = 302
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBCompany"
            Begin Extent = 
               Top = 8
               Left = 565
               Bottom = 171
               Right = 759
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2988
         Alias = 900
         Table = 2256
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBCompany"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocument"
            Begin Extent = 
               Top = 161
               Left = 265
               Bottom = 324
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocumentCategory"
            Begin Extent = 
               Top = 175
               Left = 480
               Bottom = 338
               Right = 674
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBPosition"
            Begin Extent = 
               Top = 150
               Left = 870
               Bottom = 312
               Right = 1064
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBUser"
            Begin Extent = 
               Top = 0
               Left = 633
               Bottom = 163
               Right = 827
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 135' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
USE [master]
GO
ALTER DATABASE [PretestBagusWahyu] SET  READ_WRITE 
GO
