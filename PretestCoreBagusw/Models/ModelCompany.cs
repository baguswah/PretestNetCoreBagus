﻿using System.ComponentModel.DataAnnotations;
namespace PretestCoreBagusw.Models
{
    public class ModelCompany
    {
        [Required]
        public int? ID { get; set; }
        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? Telephone { get; set; }
        public int? Flag { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

    }
}
