﻿using System.ComponentModel.DataAnnotations;
namespace PretestCoreBagusw.Models
{
    public class ModelPosition
    {
        [Required]
        public string? Name { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }

}
